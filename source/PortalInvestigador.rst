**************************************************************
Crear notificaciones en el S2i para el portal del investigador
**************************************************************

CLASE Y EXPLICACIÓN
###################

MNotificaciones
===============

Métodos
________

•	Para paso a paso:

	* int crearNotificacion (String asunto,String cuerpo)
	
	* void asociarInvNotificacion (int id_usuario,int tipo_usuario,int id_notificacion)
	
	* enviarCorreoNotificacion (int id_usuario,int tipo_usuario,String asunto,String cuerpo)

•	Para una creación completa

	*	void creacionNotificionCompleta (int id_usuario,int tipo_usuario,String asunto,String cuerpo,boolean enviar_correo)
	
FORMA 1: PASO A PASO
********************
//construcción de la clase

MNotificaciones mnot=new MNotificaciones(conn);

//creación de la notificación sin asociar a nadie

int id_notificacion=mnot.crearNotificacion (asunto_notificacion, cuerpo_notificacion);

//asociar a 1 o varios usuarios del PORTAL

mnot.asociarInvNotificacion (solicitud.getId_usuario(), solicitud.getTipo_usuario(), id_notificacion);

//NOTIFICAR EMAIL (Ejemplo que en pruebas no manda pero imprime)

if(Utilidades.getPropertie ("config_S2i","estoyEnPruebas").equals("1")){

	enviar=false;
	
	
	System.out.println ("asunto envio:"+asunto_notificacion);
	
	
	System.out.println ("cuerpo envio:"+cuerpo_notificacion);
	
	
	System.out.println ("subimos notificacion pero no email");
	
	
}else if(enviar) {

	mnot.enviarCorreoNotificacion (solicitud.getId_usuario(), solicitud.getTipo_usuario(), asunto_notificacion, cuerpo_notificacion);
	
}


FORMA 2: TODO PARA UN USUARIO	
*****************************

//construcción de la clase

MNotificaciones mnot=new MNotificaciones(conn);

//creación de la notificación, que la asocia y envía email

mnot.creacionNotificionCompleta (id_investigador, 1, asunto, cuerpo, true);		
		