.. documentacion documentation master file, created by
   sphinx-quickstart on Mon May  4 10:45:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pagina de documentacion del VIID-Informatica
============================================
.. toctree::
   :maxdepth: 3
   :caption: Contentido:

   guia
   item1
   item2
   item3
   documentaciontablas
   item4
   PortalInvestigador