*********
Titulo(1)
*********

Título(2)
=========

Título(3)
----------


Para más información acerca de links `links <https://www.sphinx-doc.org/es/master/usage/restructuredtext/basics.html#hyperlinks>`_


Notas informativas

.. important::
     Esto es una nota importante

.. note ::
     Esto es una nota 

Para items:

* item 1
* item 2
* item 3

Esto es una tabla

+---------------+------------+-----------+
| Titulo 1      | Título 2   | Título 3  |
+===============+============+===========+
| Fila normal   |  Fila      |  Fila     |
+---------------+------------+-----------+


Para texto en cursiva `cursiva`

Para texto en negrita **negrita**

Para más información acerca de `reStructuredText <https://www.sphinx-doc.org/es/master/usage/quickstart.html>`_
Para más información acerca de `Sphinx <https://www.sphinx-doc.org/es/master/usage/restructuredtext/>`_




Ejemplo de codigo
----------------------

.. code-block:: c
  :caption: this.py
  :name: this-py
  :linenos:
  :emphasize-lines: 3,5


  #include <stdio.h>

  int main()
  {
    printf("Hello World");
    return 0;
  }

.. seealso:: 
		Vea también que se pueden anidar otras directivas, pero no todas.s




.. topic::
		 Título personalizado

   El cuerpo de la caja es un párrafo indentado.


.. sidebar:: Título de la barra
    :subtitle: Subtítulo opcional

    Este párrafo indentado es el contenido
    de la barra, y se interpreta como
    elemento de body.
